using System;
using System.Collections.Generic;
using System.Text;

namespace UEIException
{
    [Serializable]
    public class LockException : BaseException
    {
        public LockException() : base()
        {

        }

        public LockException(string message) : base(message)
        {

        }

        public LockException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
