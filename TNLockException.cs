using System;
using System.Collections.Generic;
using System.Text;

namespace UEIException
{
    [Serializable]
    public class TNLockException : LockException
    {
        public TNLockException()
            : base()
        {

        }

        public TNLockException(string message)
            : base(message)
        {

        }

        public TNLockException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
