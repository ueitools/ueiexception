using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization; 

namespace UEIException
{
    [Serializable]
    public class BaseException : ApplicationException
    {
        #region Private Fields
        private string m_strMachineName = Environment.MachineName;
        #endregion

        #region Business Properties and Methods
        public string MachineName
        {
            get
            {
                return m_strMachineName;
            }
            set
            {
                m_strMachineName = value;
            }
        }
        #endregion

        #region Constructor
        public BaseException() : base()
        {

        }

        public BaseException(string message) : base(message)
        {

        }

        public BaseException(string message, Exception inner) :
            base(message, inner)
        {

        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected BaseException(SerializationInfo info, 
            StreamingContext context) : base(info,context)
        {
            m_strMachineName = info.GetString("m_strMachineName");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public override void GetObjectData(SerializationInfo info,
            StreamingContext context )
        {
            info.AddValue("m_strMachineName", m_strMachineName,
                      typeof(String));

            base.GetObjectData(info,context);
        }
    }
}
