using System;
using System.Collections.Generic;
using System.Text;

namespace UEIException
{
    [Serializable]
    public class IDLockException : LockException
    {
        public IDLockException()
            : base()
        {

        }

        public IDLockException(string message)
            : base(message)
        {

        }

        public IDLockException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
