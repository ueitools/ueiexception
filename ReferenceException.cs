using System;
using System.Collections.Generic;
using System.Text;

namespace UEIException
{
    [Serializable]
    public class ReferenceException : BaseException
    {
        public ReferenceException() : base()
        {

        }

        public ReferenceException(string message) : base(message)
        {

        }

        public ReferenceException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }

    public class DeviceTypeReferenceException : ReferenceException
    {
        public DeviceTypeReferenceException()
            : base()
        {

        }

        public DeviceTypeReferenceException(string message)
            : base(message)
        {

        }

        public DeviceTypeReferenceException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }

    public class CountryReferenceException : ReferenceException
    {
        public CountryReferenceException()
            : base()
        {

        }

        public CountryReferenceException(string message)
            : base(message)
        {

        }

        public CountryReferenceException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
    public class BrandReferenceException : ReferenceException
    {
        public BrandReferenceException()
            : base()
        {

        }

        public BrandReferenceException(string message)
            : base(message)
        {

        }

        public BrandReferenceException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
    public class OpInfoReferenceException : ReferenceException
    {
        public OpInfoReferenceException()
            : base()
        {

        }

        public OpInfoReferenceException(string message)
            : base(message)
        {

        }

        public OpInfoReferenceException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
